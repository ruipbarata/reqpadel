Submeter o documento com a lista de requisitos funcionais do vosso projecto usando Casos de Uso.
Um mínimo de TRÊS casos de uso relevantes (não, o 'login' não se inclui nesta categoria)

Incluir:

```
sCapa (projecto, equipa, nomes, contacto principal, título do trabalho, data, versão...)
Tabela de versões e revisões
Índice
Contexto/preâmbulo - para contextualizar o leitor ao projeto.
Diagrama de Contexto (nível 0)
Descrição de todos os atores (apresentados no diagrama de contexto)
Diagrama de casos de uso (nível 1)
Casos de uso detalhados:
    ID+Nome
    Nível do caso de uso usando a terminologia {cloud/kite/sea/fish/clamp}
    Fonte: identificar de onde veio este caso de uso (equipa/cliente/survey/...)
    Descrição textual sucinta do caso de uso
    Garantias mínimas (se tudo correr mal)
    Garantias de sucesso (se tudo correr bem)
    Trigger
    Ator principal -o que inicia o caso-de-uso
    Atores secundários (se aplicável)
    Pré-condições
    Descrição numerada dos passos do caso de uso (formato 'duas-colunas').
    Excepções (alguns passos podem estar "TBD" nesta versão)
    Pós-condições
```

A entrega tem de ser em formato pdf e o ficheiro deve ter o seguinte identificador:

"D3-UseCases-TEAM-NAME>_v<M.n>.pdf"

em que:

`<TEAM-NAME>` é o nome da equipa

<M.n> is the Major/miNor version number, e.g. "2.3"

OBS: cada descrição de um caso de uso deve estar contido numa página A4 (autónomo). Usem um template para que todos os UC serem apresentados de uma forma standardizada. No limite, deveria ser possivel entregar uma dessas páginas A4 a um developer para ele o implementar.



https://www.overleaf.com/project/6181c3204f0c72f40d870967
