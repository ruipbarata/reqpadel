Submit a document with the Consolidated Contextual Design Models, namely:

1.Information flow
2.Cultural model
3.Sequence (only of relevant tasks)
4.Physical (optional)
5.Artefact (optional)
6.Personnas (at least one)

Include the identification of the actual users you interviewed, and their profile (on what relates to the project).

Also include, as an annex, the first affinity notes you collected during the consolidation phase; include a picture of it.

Treat this is a formal artefact: don't forget to include a front page with your project name, team members, contacts, version number, number the pages, use relevant header and footers, etc. State your ‘mantra’ in the cover page.

Filename:

ER2021_`<TeamName>`_ContextualDesignModels_v1.0.pdf



https://www.overleaf.com/project/616c2fcc95c38b9cc5b9bfa1
