Submeter o documento com a lista de requisitos do vosso projecto usando uma abordagem baseada em objectivos (goals) usando a linguagem de modelação i*

Este trabalho deve focar-se na identificação de requisitos **não-funcionais** do sistema (os requisitos funcionais já foram o foco do trabalho sobre casos-de-uso).

Incluir:

```
Capa (projecto, equipa, nomes, contacto principal, título do trabalho, data, versão...)
Tabela de versões e revisões
Índice
Contexto/preâmbulo - para contextualizar o leitor ao projeto.
**Link para o repositório gitlab onde se encontrem as sources do documento.**
Modelos dos vários níveis de abstracção.
→ Uma strategic dependency view (só atores e dependências)
→ Uma strategic rationale view (todos os atores e dependências)
→ Uma strategic rationale view POR CADA UM dos actores principais (isoladamente), para se perceber melhor a lógica interna de cada ator.

Anexos:
A: a lista de requisitos não-funcionais identificados.
B: a definição de cada um dos requisitos não-funcionais identificados no anexo A.
```

O trabalho deve ser entregue em formato PDF e o ficheiro deve ter o seguinte identificador:

"D4-GoalModels-TEAM-NAME>_v<M.n>.pdf"

em que:

`<TEAM-NAME>` é o nome da equipa

<M.n> is the Major/miNor version number, e.g. "2.3"



https://www.overleaf.com/project/6194ead2d7dba14092d51f95
